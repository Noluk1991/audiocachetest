//
//  Podcast.m
//  AudioCache
//
//  Created by Александр Андрюхин on 19.07.16.
//  Copyright © 2016 AlexanderAndriukhin. All rights reserved.
//

#import "Podcast.h"

@implementation Podcast

- (instancetype)init {
    return [self initWithResponse:nil];
}

- (instancetype)initWithResponse:(NSDictionary *)response {
    self = [super init];
    if (self) {
        [self setupInitialValues];
        if (response) {
            self.name = response[@"name"];
            self.imageUrlString = response[@"image"];
            NSLog(@"%@", self.imageUrlString);
            self.audioUrlString = [response[@"play"] objectForKey:@"high"];
            self.idString = response[@"id"];
        }
    }
    return self;
}

- (void)setupInitialValues {
    self.name = @"default name";
    self.imageUrlString = nil;
    self.audioUrlString = nil;
}

- (BOOL)hasImage {
    return ![self.imageUrlString isKindOfClass:[NSNull class]] ? YES : NO;
}

@end