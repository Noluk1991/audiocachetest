//
//  Podcast.h
//  AudioCache
//
//  Created by Александр Андрюхин on 19.07.16.
//  Copyright © 2016 AlexanderAndriukhin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Podcast : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageUrlString;
@property (nonatomic, strong) NSString *audioUrlString;
@property (nonatomic, strong) NSString *localPath;
@property (nonatomic, strong) NSString *idString;
@property (nonatomic, assign) BOOL isPlaying;

- (instancetype)initWithResponse:(NSDictionary *)response NS_DESIGNATED_INITIALIZER;

- (BOOL)hasImage;

@end