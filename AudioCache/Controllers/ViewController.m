//
//  ViewController.m
//  AudioCache
//
//  Created by Александр Андрюхин on 19.07.16.
//  Copyright © 2016 AlexanderAndriukhin. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "ViewController.h"
#import "ServerManager.h"
#import "PodcastCell.h"
#import "Podcast.h"
#import "MBProgressHUD.h"

@import AVFoundation;

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIProgressView *currentProgressView;
@property (nonatomic, strong) Podcast *currentPlayingPodcast;
@property (nonatomic, strong) NSTimer *podcastTimer;

@property (nonatomic, strong) AVPlayer *player;

@property (nonatomic, strong) NSArray *podcasts;

@property (nonatomic, strong) NSString *selectedSongUrl;

@end

@implementation ViewController

@synthesize player;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self loadPoscasts];
}

#pragma mark - setup view

- (void)setupTableView {
    self.tableView.estimatedRowHeight = 40;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc]init];
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
}

- (void)loadPoscasts {
    __weak ViewController *weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    ServerManager *sharedManager = [ServerManager sharedManager];
    [sharedManager loadInfoWithCompletion:^(NSArray *podcasts, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        weakSelf.podcasts = podcasts;
        [weakSelf.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView delegate and data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.podcasts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    PodcastCell *cell = [self configurePodcastCellAtIndexPath:indexPath];
    if (!cell) {
        cell = [[PodcastCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"podcastCell"];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Podcast *choosenPodcast = [self.podcasts objectAtIndex:indexPath.row];
    PodcastCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if (self.player.rate == 1.0f) {
        if (choosenPodcast.isPlaying == YES) {
            choosenPodcast.isPlaying = NO;
            [self stopPlayer];
        
        } else {
            // already playing another podcast
            [self stopPlayer];
            self.currentProgressView = cell.progressView;
            self.currentPlayingPodcast.isPlaying = NO;
            self.currentPlayingPodcast = choosenPodcast;
            [self playPodcast:choosenPodcast];
        }
    } else {
        self.currentProgressView = cell.progressView;
        [self playPodcast:choosenPodcast];
    }
}

- (PodcastCell *)configurePodcastCellAtIndexPath:(NSIndexPath *)indexPath {
    Podcast *podcast = [self.podcasts objectAtIndex:indexPath.row];
    static NSString *cellIdentifier = @"podcastCell";
    
    PodcastCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (podcast.hasImage) {
        [cell.podcastImageView sd_setImageWithURL:[NSURL URLWithString:podcast.imageUrlString]
                                 placeholderImage:[UIImage imageNamed:@"placeholder"]];
    } else {
        cell.podcastImageView.image = [UIImage imageNamed:@"placeholder"];
    }
    cell.podcastImageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.podcastImageView.clipsToBounds = YES;
    cell.podcastLabel.text = podcast.name;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.downloadButton addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.progressView.hidden = !podcast.isPlaying;
    BOOL isDownloaded = !podcast.localPath ? YES : NO;
    [cell.downloadButton setEnabled:isDownloaded];
    return cell;
}

- (void)checkButtonTapped:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    Podcast *currentPodcast = [self.podcasts objectAtIndex:indexPath.row];
    if (indexPath != nil) {
        [self downloadPodcast:currentPodcast sender:sender];
    }
}

- (void)downloadPodcast:(Podcast *)podcast sender:(UIButton *)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Загрузка";
    
    [[ServerManager sharedManager] loadFileWithUrl:podcast.audioUrlString withName:podcast.idString progress:^(NSProgress *progress) {
        hud.progress = (float)progress.fractionCompleted;
    } completion:^(NSString *result, NSError *error) {
        podcast.localPath = result;
        [hud hide:YES];
        [sender setEnabled:NO];
    }];
}

#pragma mark - player methods

-(void)playPodcastWithUrl:(NSString *)urlString {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:urlString]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.player currentItem]];
    [self.player addObserver:self forKeyPath:@"status" options:0 context:nil];
    self.podcastTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];
}

- (NSTimeInterval) availableDuration {
    NSArray *loadedTimeRanges = [[self.player currentItem] loadedTimeRanges];
    CMTimeRange timeRange = [[loadedTimeRanges objectAtIndex:0] CMTimeRangeValue];
    Float64 startSeconds = CMTimeGetSeconds(timeRange.start);
    Float64 durationSeconds = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result = startSeconds + durationSeconds;
    return result;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    if (object == player && [keyPath isEqualToString:@"status"]) {
        if (player.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
        } else if (player.status == AVPlayerStatusReadyToPlay) {
            [self.player play];
        } else if (player.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
        }
    }
}

- (void)updateProgress:(NSTimer *)timer {
    if (self.player.status == AVPlayerStatusReadyToPlay) {
        CGFloat allTime = CMTimeGetSeconds([[self.player currentItem] duration]);
        CGFloat currentTime = CMTimeGetSeconds([self.player currentTime]);
        self.currentProgressView.progress = (float)(currentTime/allTime);
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self stopPlayer];
}

- (void)playPodcast:(Podcast *)podcast {
    podcast.isPlaying = YES;
    self.currentProgressView.hidden = NO;
    self.currentPlayingPodcast = podcast;
    NSString *podcastUrlString = podcast.audioUrlString;
    if (podcast.localPath) {
        podcastUrlString = podcast.localPath;
    }
    [self playPodcastWithUrl:podcastUrlString];
}

- (void)stopPlayer {
    self.currentPlayingPodcast.isPlaying = NO;
    self.currentProgressView.progress = 0;
    self.currentProgressView.hidden = YES;
    self.currentProgressView = nil;
    [self.player pause];
    [self.player removeObserver:self forKeyPath:@"status"];
    self.player = nil;
    [self.podcastTimer invalidate];
    self.podcastTimer = nil;
}

@end
