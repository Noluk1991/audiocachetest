//
//  ServerManager.h
//  AudioCache
//
//  Created by Александр Андрюхин on 19.07.16.
//  Copyright © 2016 AlexanderAndriukhin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerManager : NSObject

+ (ServerManager *)sharedManager;

- (void)loadInfoWithCompletion:(void (^) (NSArray *podcasts, NSError *error))completion;
- (void)loadFileWithUrl:(NSString *)stringUrl withName:(NSString *)name progress:(void (^) (NSProgress *progress))progress completion:(void (^) (NSString *result, NSError *error))completion;
@end
