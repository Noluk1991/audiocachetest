//
//  ServerManager.m
//  AudioCache
//
//  Created by Александр Андрюхин on 19.07.16.
//  Copyright © 2016 AlexanderAndriukhin. All rights reserved.
//

#import "ServerManager.h"
#import <AFNetworking/AFNetworking.h>
#import "Podcast.h"

@interface ServerManager ()

@end

@implementation ServerManager

+ (ServerManager *)sharedManager {
    static ServerManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc]init];
    });
    return manager;
}

- (void)loadInfoWithCompletion:(void (^) (NSArray *podcasts, NSError *error))completion {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager GET:@"https://backend.soundstream.media/API/v1.5/?action=get_podcasts" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success");
        NSArray *responseDataArray = [[responseObject objectForKey:@"data"] allObjects];
        
        NSMutableArray *podcasts = [[NSMutableArray alloc]init];
        
        for (NSDictionary *podcastDictionary in responseDataArray) {
            Podcast *newPodcast = [[Podcast alloc]initWithResponse:podcastDictionary];
            [podcasts addObject:newPodcast];
        }
        completion(podcasts, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"failure");
    }];
}

- (void)loadFileWithUrl:(NSString *)stringUrl withName:(NSString *)name progress:(void (^) (NSProgress *progress))progress completion:(void (^) (NSString *result, NSError *error))completion {
   // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:stringUrl]];
    

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    
    NSURL *URL = [NSURL URLWithString:stringUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        progress(downloadProgress);
    } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3", name]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        completion(filePath.absoluteString, nil);
    }];
    [downloadTask resume];
}

@end


//+ (id)sharedInstance {
//    static ParseHelperClass *sharedHelper = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedHelper = [[self alloc] init];
//    });
//    return sharedHelper;
//}
//
//- (id)init {
//    self = [super init];
//    if (self) {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
//        self.internetReachability = [Reachability reachabilityForInternetConnection];
//        [self checkReachabilityStatus];
//        [self.internetReachability startNotifier];
//    }
//    return self;
//}