//
//  PodcastCell.h
//  AudioCache
//
//  Created by Александр Андрюхин on 19.07.16.
//  Copyright © 2016 AlexanderAndriukhin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PodcastCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *podcastImageView;
@property (nonatomic, weak) IBOutlet UILabel *podcastLabel;
@property (nonatomic, weak) IBOutlet UIButton *downloadButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@end